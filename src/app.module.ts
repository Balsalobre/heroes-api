import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HeroModule } from './hero/hero.module';
import { CountryModule } from './country/country.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';

const USER = "bls";
const PASSWORD = "4rC5jNvbFSfBHTr1";
const DATA_BASE = "hero-api";

const CONECTOR = `mongodb+srv://${USER}:${PASSWORD}@cluster0.dyy7c.mongodb.net/${DATA_BASE}?retryWrites=true&w=majority`;

@Module({
  imports: [
    AuthModule,
    CountryModule,
    HeroModule,
    // MongooseModule.forRoot('mongodb://localhost/dana-heroes',
    //   { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
    MongooseModule.forRoot(CONECTOR,
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
